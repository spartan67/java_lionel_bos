package App;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Helpers.Binary;
import Helpers.XML;
import Model.Adresse;

/*
 * POJO stands for Plain Old Java Object.
 * 
 * It is an ordinary Java object, not bound by any special restriction other than those forced
 * by the Java Language Specification and not requiring any classpath.
 * POJOs are used for increasing the readability and re-usability of a program.
 * POJOs have gained the most acceptance because they are easy to write and understand.
 * They were introduced in EJB 3.0 by Sun microsystems.
 *  
 *  
 * A POJO should not:
 * 
 * - Extend prespecified classes
 * - Implement prespecified interfaces
 * - Contain prespecified annotations

 * Beans are special type of Pojos. There are some restrictions on POJO to be a bean.
 * 
 * All JavaBeans are POJOs but not all POJOs are JavaBeans.
 * Serializable i.e. they should implement Serializable interface. Still,
 * some POJOs who don�t implement a Serializable interface are called POJOs
 * because Serializable is a marker interface and therefore not of many burdens.
 * 
 * - Fields should be private. This is to provide complete control on fields.
 * - Fields should have getters or setters or both
 * - A no-arg constructor should be there in a bean.
 * - Fields are accessed only by constructor or getter setters.
 */

public class App {

	public static void main(String[] args) {

		Adresse adresse = new Adresse("Arthur Weber", "Strasbourg", 67000);
		Adresse adresse2 = new Adresse("Place Kleber", "Strasbourg", 67000);

		ArrayList<Adresse> list = new ArrayList<Adresse>();

		list.add(adresse);
		list.add(adresse2);

		try {

			XML.encodeToFile(adresse, "Exports/adresse.xml");

			Adresse adresseLue = (Adresse) XML.decodeFromFile("Exports/adresse.xml");

			System.out.println(adresseLue);

			Binary.encodeToFile(list, "Exports/adresse.bin");

			ArrayList<?> arrayList = (ArrayList<?>) Binary.decodeFromFile("Exports/adresse.bin");

			for (int i = 0; i < arrayList.size(); i++) {
				System.out.println(arrayList.get(i));
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
