package Model;

import java.io.Serializable;


/*
 * POJO stands for Plain Old Java Object.
 * It is an ordinary Java object, not bound by any special restriction other than those forced
 * by the Java Language Specification and not requiring any classpath.
 * POJOs are used for increasing the readability and re-usability of a program.
 * POJOs have gained the most acceptance because they are easy to write and understand.
 * They were introduced in EJB 3.0 by Sun microsystems.
 *  
 *  
 * A POJO should not:
 * 
 * - Extend prespecified classes
 * - Implement prespecified interfaces
 * - Contain prespecified annotations

 * Beans are special type of Pojos. There are some restrictions on POJO to be a bean.
 * 
 * All JavaBeans are POJOs but not all POJOs are JavaBeans.
 * Serializable i.e. they should implement Serializable interface. Still,
 * some POJOs who don�t implement a Serializable interface are called POJOs
 * because Serializable is a marker interface and therefore not of many burdens.
 * 
 * - Fields should be private. This is to provide complete control on fields.
 * - Fields should have getters or setters or both
 * - A no-arg constructor should be there in a bean.
 * - Fields are accessed only by constructor or getter setters.
 */

public class Adresse implements Serializable{

	private static final long serialVersionUID = 1L;

	private String voie;
	
	private String ville;
	
	// Mot cl� "transient" : champ ignor� lors de la s�rialisation de l'objet
	private transient int codePostal;

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public int getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	// ATTENTION A NE PAS OUBLIER LE CONSTRUCTEUR SANS PARAMETRE POUR LES ENCODERS
	public Adresse() {
		super();
	}
	
	public Adresse(String voie, String ville, int codePostal) {
		super();
		this.voie = voie;
		this.ville = ville;
		this.codePostal = codePostal;
	}

	@Override
	public String toString() {
		return "Adresse [voie=" + voie + ", ville=" + ville + ", codePostal=" + codePostal + "]";
	}
}
