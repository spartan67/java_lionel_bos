package _01_User;

public class IllegalAgeException extends IllegalArgumentException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalAgeException()
	{
		super("Un age ne peut pas �tre n�gatif");
	}
}
