package _02_Parking;

public class Parking {

	private static int nombreDePlacesOccupees;
	
	private int nombreDePlaces;

	public int getNombreDePlaces() {
		return nombreDePlaces;
	}

	public void setNombreDePlaces(int nombreDePlaces) {
		this.nombreDePlaces = nombreDePlaces;
	}

	public Parking(int nombreDePlaces) {
		super();
		this.nombreDePlaces = nombreDePlaces;
	}
	
	public int getNombreDePlacesDisponibles()
	{
		return nombreDePlaces - nombreDePlacesOccupees;
	}
	
	public void remplir() throws ParkingPleinException
	{
		if(nombreDePlacesOccupees >= nombreDePlaces)
		{
			throw new ParkingPleinException();
		}
		
		nombreDePlacesOccupees++;
	}
}
