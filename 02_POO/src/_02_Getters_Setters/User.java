package _02_Getters_Setters;

public class User {

	// Membres d'instances
	private String nom;
	private String prenom;
	private int age;

	// Membre de classe
	private static int nUsers;

	// Getters and Setters

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return this.age;
	}
	
	public void setAge(int age) {
		
		if (age <= 0) {
			System.out.println("Un age ne peut pas �tre n�gatif"); // En vrai : on l�verait une Exception...
		} else {
			this.age = age;
		}
	}

	public static int getnUsers() {
		return nUsers;
	}

	// Constructeurs
	public User() {
		nUsers++;
	}

	public User(String nom, String prenom) {

		this();
		this.nom = nom;
		this.prenom = prenom;

	}

	public User(String nom, String prenom, int age) {

		this(nom, prenom);
		this.age = age;
	}

	// M�thodes
	@Override
	public String toString() {
		return prenom + " " + nom + " a " + age + " ans.";
	}
}
