package _02_Getters_Setters;

public class App {

	public static void main(String[] args) {
		
		User user = new User("Duck", "Riri", 12);
		
		System.out.println(user);
		
		// user.age = -12; // les propri�t�s sont accessibles directement depuis le monde ext�rieur : Ne respecte pas le principe d'encapsulation
	 
		// Pour r�oudre le probl�me, on va utiliser des getters et setters...
		user.getAge();
		
		user.setAge(-12);
		
		System.out.println(user);
		
		user.setAge(13);
		
		user.setPrenom("Fifi");
		
		System.out.println(user);
		
		// User.nUsers = -89;
		
		System.out.println("Nombre d'utilisateurs : " + User.getnUsers());
	}
}
