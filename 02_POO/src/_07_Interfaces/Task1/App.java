package _07_Interfaces.Task1;

public class App {

	public static void main(String[] args) {

		// Le polymorphisme s'applique aussi aux interfaces
		Animal chien = new Chien();
		Animal chat = new Chat();
		
		chien.communiquer();
		chat.communiquer();
	}
}
