package _07_Interfaces.Task1;


/*
 * Une interface est un contrat que toute classe qui impl�mente cette interface devra respecter.
 * En java une interface n'est pas une classe, mais un ensemble d'exigences (de fonctionnait�s) pour les classes
 * qui souhaitent s'y conformer.
 * Une interface se pr�sente sous la forme d'un ensemble de m�thodes abstraites.
 * Contrairement � une classe, une interface ne peut pas avoir de constructreur ou d'attributs.
 * Une interface ne peut compter que des champs "static" ou d�finis avec le mot cl� "final"
 */
public interface Animal {

	void communiquer();
}
