package _01_Constructeur;

public class App {

	public static void main(String[] args) {

		System.out.println("Nombre d'utilisateurs : " + User.nUsers);
		
		User user1 = new User(); // Appel du constructeur sans param�tre de la classe "User" pour instancier un objet nomm� "user1"
		
		System.out.println("Nombre d'utilisateurs : " + User.nUsers);
		
		User user2 = new User("Duck", "Riri");
		
		System.out.println(user2.nom);
		System.out.println(user2.prenom);
		
		System.out.println("Nombre d'utilisateurs : " + User.nUsers);
		
		User user3 = new User("Duck", "Fifi", 12);
		
		System.out.println("Nombre d'utilisateurs : " + User.nUsers);
		
		System.out.println(user3/*.toString()*/);
		
		user3.age = -12; // PROBLEME : les propri�t�s de mes Users sont directement accessibles depuis le monde ext�rieur
		
		System.out.println(user3);
	}
}
