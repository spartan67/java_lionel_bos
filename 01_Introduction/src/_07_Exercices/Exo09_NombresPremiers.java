package _07_Exercices;

import java.util.ArrayList;
import java.util.Scanner;

public class Exo09_NombresPremiers {

	/*
	 * Le probl�me est de d�terminer si un nombre entier est un nombre premier.
	 * D�finition : Un nombre premier est un nombre :
	 * - strictement sup�rieur � 1
	 * - qui n�est divisible (au sens de la division enti�re) que par 1 et par lui-m�me.
	 * 
	 * Ainsi :
	 * 2 est un nombre premier (il est uniquement divisible par 1 et par lui-m�me)
	 * 3 aussi
	 * 4 n�est pas premier (il est divisible par 2)
	 * 5 est premier
	 * etc...
	 * 
	 * Le nombre 1 pourrait �tre consid�r� comme un nombre premier (il n�est pas divisible sauf par 1 et par lui-m�me).
	 * Il est exclu de la liste des nombres premiers par commodit�, car il ne respecte pas certaines lois communes � tous les autres nombres premiers.
	 * La m�thode de base consiste � prouver que le nombre � traiter n�est pas premier en recherchant un diviseur qui donne un reste �gal � 0.
	 * Si on en trouve un, le nombre n�est pas premier.
	 * Si aucun diviseur n�est trouv�, il s�agit d�un nombre premier.
	 * 
	 * Ecrivez un premier programme qui effectue ce calcul. On utilisera une boucle FOR afin de tester tous les diviseurs de 2 au nombre � 1.
	 * Le programme demandera un nombre entier et indiquera s�il est premier ou non.
	 */
	
	public static Boolean nombrePremier(int nombre)
	{
		for(int i = 2; i < nombre; i++)
		{
			if(nombre % i == 0) return false;
		}
		return true;
	}
	
	public static ArrayList<Integer> diviseurs(int nombre)
	{
		ArrayList<Integer> liste = new ArrayList<Integer>();
		
		for(int i = 2; i < nombre; i++)
		{
			if(nombre %i == 0)
			{
				liste.add(i);
			}
		}
		
		return liste;
	}
	
	public static void main(String[] args) {
		
		Scanner clavier = new Scanner(System.in);

		System.out.print("Saisir un nombre entier positif : ");
		
		int nombre = clavier.nextInt();
		
		if(nombrePremier(nombre))
		{
			System.out.println(nombre + " est un nombre premier");
		}
		else
		{
			System.out.println(nombre + " n'est pas un nombre premier");
			
			ArrayList<Integer> liste = diviseurs(nombre);
			
			for(int i = 0; i < liste.size(); i++)
			{
				System.out.println(liste.get(i) + " divise " + nombre);
			}
		}
		
		clavier.close();
	}
}
