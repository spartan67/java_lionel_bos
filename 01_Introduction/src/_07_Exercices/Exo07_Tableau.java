package _07_Exercices;

import java.util.Scanner;

public class Exo07_Tableau {

	/*
	 * Insertion dans un tableau tri�
	 * 
	 * Il faut ins�rer la nouvelle donn�e directement � la bonne place. C�est le
	 * m�canisme de base du tri par insertion. L�insertion d�une donn�e � une place
	 * pr�cise lib�re la case qui doit recevoir la donn�e, en d�calant d�une case
	 * toute la partie droite du tableau � partir de cette position, puis copie la
	 * donn�e dans la case lib�r�e. Pour �viter toute perte de donn�es, le d�calage
	 * part de la fin pour remonter ver la position d�insertion. Le d�calage d�bute
	 * � la derni�re case qui contient une donn�e, se poursuit sur l�avant-derni�re
	 * case, et ainsi de suite.
	 * 
	 * Cr�er et initialiser un tableau, puis ins�rer un �l�ment � la position
	 * sp�cifi�e dans ce tableau (de 0 � N-1). Pour ins�rer un nouvel �l�ment dans
	 * le tableau, d�placez les �l�ments de la position d'insertion donn�e vers une
	 * position vers la droite.
	 * 
	 * Exemple Donn�es d'entr�e : Saisir le nombre de notes : 4 Note 1 : 8.5 Note 2
	 * : 9.5 Note 3 : 12.5 Note 4 : 18.0 Saisir la note 5 : 11.0 Donn�es de sortie :
	 * [8.5, 9.5, 11.0, 12.5, 18.0]
	 */

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);

		System.out.print("Saisir le nombre de notes : ");

		int nNotes = clavier.nextInt();

		double[] notes = new double[nNotes + 1];

		for (int i = 0; i < nNotes; i++) {

			System.out.print("Note " + (i + 1) + " : ");

			notes[i] = clavier.nextDouble();
		}

		System.out.print("Saisir la nouvelle note " + (nNotes + 1) + " : ");

		double newNote = clavier.nextDouble();

		int newPos = 0;

		for (int i = 0; i < notes.length; i++) {
			if (notes[i] < newNote) {
				newPos++;
			}
		}

		for (int i = nNotes; i > newPos - 1; i--) {
			notes[i] = notes[i - 1];
		}

		notes[newPos - 1] = newNote;

		System.out.println("Donn�es de sorties : ");

		for (int i = 0; i <= nNotes; i++) {
			System.out.println(notes[i]);
		}

		// On peut faire beaucoup plus simple avec une liste !
		clavier.close();
	}
}
