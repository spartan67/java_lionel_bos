package _07_Exercices;

import java.util.Scanner;

public class Exo06_Boucles {

	/*
	 * L'utilisateur doit deviner un nombre secret (g�n�r� automatiquement) entre 1
	 * et 1000. On va donc demander � l'utilisateur de trouver ce nombre secret.
	 * Tant qu'il n'a pas trouv� ce nombre on lui demandera encore et encore,
	 * jusqu'� ce qu'il le trouve. Si l'utilisateur choisit un nombre trop petit,
	 * l'application lui dira que le nombre qu�il a rentr� est trop petit Si
	 * l'utilisateur choisit un nombre trop grand, l'application lui dira que le
	 * nombre qu�il a rentr� est trop grand Si l'utilisateur trouve le nombre
	 * recherch�, l'application lui indiquera le nombre d'essais dont il a eu besoin
	 * 
	 * Exemple : Entr�e : Entrer un nombre entre 1 et 1000 (1 - 1000) : 500 Sorties
	 * possibles : Votre nombre est trop grand ! Votre nombre est trop petit !
	 * Trouv� en 8 essais ! Bien Jou� !
	 */

	public static void main(String[] args) {

		int numberToFind = (int) (Math.random() * 999) + 1;
		int count = 1;
		
		Scanner clavier = new Scanner(System.in);

		System.out.print("Veuillez entrer un entier entre 1 et 1000 :");
		
		int choice = clavier.nextInt();

		do
		{
			if(choice > numberToFind)
			{
				System.out.print("Votre nombre est trop grand! Essayez encore : ");
			}
			
			if(choice < numberToFind)
			{
				System.out.print("Votre nombre est trop petit! Essayez encore : ");
			}
			
			choice = clavier.nextInt();
			
			count++;
		}
		while(numberToFind != choice);
		
		System.out.println("Trouv� en " + count + " essais !");

		clavier.close();
	}
}
