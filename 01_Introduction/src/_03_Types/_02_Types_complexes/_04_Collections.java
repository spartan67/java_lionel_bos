package _03_Types._02_Types_complexes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class _04_Collections {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<String>();
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		
		System.out.println("**************** Parcours for classique *****************");
		for(int i = 0; i < list.size(); i++)
		{
			System.out.println(list.get(i));
		}
		
		System.out.println("**************** Parcours foreach *****************");
		
		for (String str : list) {
			System.out.println(str);
		} 
		
		/*
		 * La classe "Collections" offrent plusieur m�thodes utilitaires pour les objets de type collection
		 */
		
		System.out.println("Classe Collections");
		System.out.println(list + " (list)");
		
		Collections.shuffle(list);
		System.out.println(list + " (Collections.shuffle)");
		
		Collections.sort(list);
		System.out.println(list + " (Collections.sort)");
		
		Collections.rotate(list, 1);
		System.out.println(list + " (Collections.rotate 1)");
		
		Collections.rotate(list, -1);
		System.out.println(list + " (Collections.rotate -1)");
		
		Collections.reverse(list);
		System.out.println(list + " (Collections.reverse)");
		
		List<String> sousListe = list.subList(2, 5);
		System.out.println(sousListe + " (list.subList(2, 5))");
		
		
		System.out.println("\n**************** Tableaux associatifs *********************");
		/*
		 * Un tableau associatif (parfois appel� dictionnaire) ou "map" permet d'associer une cl� � une valeur
		 * Un tableau associatif ne peut pas contenir de doublon de cl�.
		 */
		
		Map<Integer, String> map = new TreeMap<Integer, String>();
		// Map<Integer, String> map = new HashMap<Integer, String>();
		// Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		
		map.put(1, "un");
		map.put(2, "deux");
		map.put(3, "trois");
		map.put(4, "quatre");
		map.put(5, "cinq");
	    map.put(1, "doublon"); // la cl� existe d�j� => l'entr�e sera �cras�e par la nouvelle valeur
		
		System.out.println("\nkeySet() : retourne un objet de type Set<> repr�sentant la liste des cl�s contenues dans la collection");
		for(Integer item : map.keySet()) 
		{
			System.out.print(item + " ");
		}
		
		System.out.println();
		
		System.out.println("\nentrySet() : retourne un objet de type Map.Entry<,> repr�sentant la liste des �l�ments contenus dans la collection");
		for(Map.Entry<Integer, String> entry : map.entrySet())
		{
			Integer key = entry.getKey();
			String value = entry.getValue();
			
			System.out.println("cl� : " + key + " - valeur : " + value);
		}
		
		System.out.println("\nLa m�thode remove permet de supprimer un �l�ment � partir de sa cl�");
		
		System.out.println(map + " (avant suppression)");
		
		map.remove(2);
		
		System.out.println(map + " (apr�s suppression cl� 2)");
	}
}
