package _04_Operateurs;

public class _01_Arithmetiques {

	public static void main(String[] args) {

		int v1 = 4;
		int v2; // variable non initialis�e

		v2 = 10; // In faut initialiser une variable avant de pouvoir s'en servir

		System.out.println("sum : " + (v1 + v2));
		System.out.println("sub : " + (v1 - v2));
		System.out.println("prod : " + (v1 * v2));
		System.out.println("div : " + (v2 / v1));
		System.out.println("modulo : " + (v2 % v1)); // modulo : reste de la division enti�re
	}
}
