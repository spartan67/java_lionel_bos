package _04_Operateurs;

public class _02_Affectation {

	public static void main(String[] args) {

		int c = 10;

		System.out.println(c); // 10
		System.out.println(c++); // 10 : �crit puis incr�mente
		System.out.println(++c); // 12 : incr�mente � partir de 11 puis �crit

		int a = 0;
		System.out.println("a = " + a);
		
		a += 5;
		System.out.println("a = " + a);
		
		a *= 5;
		System.out.println("a = " + a);
		
		a /= 5;
		System.out.println("a = " + a);
		
		a -= 5;
		System.out.println("a = " + a);
	}
}
