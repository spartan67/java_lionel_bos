package _01_MyFirstJavaApp;

/**
 * @author Admin stagiaire
 *
 */
public class HelloWorld {

	/**
	 * Ceci est un  commenaire qui sera utilis� pour g�n�rer de la documenation automatique par JavaDoc
	 * @param args
	 */
	public static void main(String[] args) {

		// Ceci est un commentaire sur une seule ligne
		
		System.out.println("Hello World !");
		System.out.println("Hello World Again !");
		
		/*
		 * Ceci est un commentaire
		 * sur 
		 * plusieurs lignes
		 */
	}
}
