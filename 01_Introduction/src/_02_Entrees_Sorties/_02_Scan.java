package _02_Entrees_Sorties;

import java.util.Scanner;

public class _02_Scan {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);

		System.out.println("Entrer un nombre entier : ");

		int n = clavier.nextInt();

		System.out.println("Le carr� de " + n + " vaut " + n * n);
		
		System.out.println("Entrer un nombre � virgule flottante : ");

		double d = clavier.nextDouble(); // Attention, il faut entrer la virgule sous forme de virgule... sinon exception

		System.out.println("Vous avez entr� " + d);
		
		System.out.println("Entrez une phrase svp...");
		
		String str = clavier.next(); // Ne r�cup�re qu'un mot...
		str += clavier.nextLine();   // R�cup�re toute la ligne
		
		System.out.println("Entrez une deuxi�me phrase svp...");
		
		str = clavier.nextLine();   // R�cup�re toute la ligne directement
		
		System.out.println(str);

		clavier.close();
	}
}
