package _05_Instructions;

public class _01_General {

	public static void main(String[] args) {
		
		/*
		 * Une instruction simple se termine par un ";"
		 * 
		 * Un bloc d'instruction est contenu entre accolades "{bloc d'instructions}"
		 * 
		 * Les variables d�clar�es dans un bloc d'instruction ne sont accessibles que dans ledit bloc.
		 */
		
		System.out.println("Cette instruction se termine forc�ment par un \";\"");
		
		{
			System.out.println("Ceci est la premi�re instruction d'un bloc d'instructions");
			
			int a = 10; // la variable a n'est accessible que dans ce bloc d'instructions
			
			System.out.println("Ceci est la derni�re instruction d'un bloc d'instructions");
		}
		
		 // a = 12; //Erreur la variable a n'existe plus ici...
	}
}
